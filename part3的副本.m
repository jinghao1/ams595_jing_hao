function output=Pi(c)%define a function, which can be used to calculate the value of pi
m=100000;%the maxium number of the random point
e_pi=ones(1,m);
d=100;%the length of the step
precision=1;%set the precision which is bigger than the defined precision
n0=1000;
n=n0;
while precision>=c
    k=0;
    x=rand(1,n);
    y=rand(1,n);
    for i=1:n
        if x(i)^2+y(i)^2<=1
            k=k+1;
        end
    end
    format long;
    e_pi(n)=4*k/n;
    precision=abs(e_pi(n)-e_pi(n-100));
    n=n+d;
    PI=e_pi(n-100)%the estimate value of pi
    h1=zeros(1,n)-10;%to define some zeros arrays to help classify the points inside and outside the circle
    j1=zeros(1,n)-10;
    h2=zeros(1,n)-10;
    j2=zeros(1,n)-10;
    for i=1:n-100
        if x(i)^2+y(i)^2<=1
            h1(i)=x(i);%when the points fall into the unit circle, their value will be stored
            j1(i)=y(i);
        else h2(i)=x(i);
            j2(i)=y(i);
        end
    end
    e1=find(h1>-10);
    e2=find(h2>-10);
    n1=size(e1);
    n2=size(e2);
    w1=ones(1,n1(2));
    v1=ones(1,n2(2));
    w2=ones(1,n2(2));
    v2=ones(1,n2(2));
    for i=1:n1(2)
        w1(i)=x(e1(i));
        v1(i)=y(e1(i));
    end
    for i=1:n2(2)
        w2(i)=x(e2(i));
        v2(i)=y(e2(i));
    end
    plot(w1,v1,'r*')
    hold on；
    plot(w2,v2,'bo')
    title(figure4)
    xlabel('x');ylabel('y')
    drawnow
    hold off
   end
    
   
    
            
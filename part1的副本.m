a=10000;%the maxium number of this test 
diff=ones(1,a);%diff represent the difference between the true value of pi and the computed pi
time=ones(1,a);%time represent the execution time 
for n=1:a
    tic;%tic and toc functions can calculate code that runs for a while
    k=0;
    x=rand(1,n);%x could be the random number in the interval(0,1)
    y=rand(1,n);%y could be the random number in the interval(0,1)
    for i=1:n
        if x(i)^2+y(i)^2<=1
            k=k+1;
        end
    e_pi=4*k/n;%using the 
    diff(n)=e_pi-pi;
    time(n)=toc;
    end
end
plot(1:a,diff);
title('Difference vs number of random points')
xlabel('the number of random number')
ylabel('precision')
figure(2)
plot(diff,time,'.')
title('precision vs the computational cost')
xlabel('precision')
ylabel('execution time')


    
c=input('level')%% input function help the users define the different level of precision
m=100000;%make the maxium number of random points
e_pi=ones(1,m);
d=100;%length of the step
precision=1;%define a precision which is bigger than the precision user want
n0=1000;
n=n0;
while precision>=c
    k=0;
    x=rand(1,n);
    y=rand(1,n);
    for i=1:n
        if x(i)^2+y(i)^2<=1
            k=k+1;
        end
    end
    format long;
    e_pi(n)=4*k/n;
    precision=abs(e_pi(n)-e_pi(n-100));%Calculate a rough precision
    n=n+d;
end
d%the length of the step
(n-n0-100)/100%steps for the different level of precision
Pi=e_pi(n-100)%value of pi







